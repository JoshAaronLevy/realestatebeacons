var DEFAULT_UPLOAD_URI = 'http://virtualmls.com/REBeacons/upload.aspx';

var PaymentAdminController = module.exports = ["$http", "$scope", "$sce", "apiService", "paymentService", "paymentAdminService", function($http, $scope, $sce, apiService, paymentService, paymentAdminService) {

  $scope.trustAsHtml = $sce.trustAsHtml;

  $scope.load = function() {
    paymentAdminService.get();
  };

  $scope.editPayment = function() {
    var item = $scope.$root.payment;
    return apiService
    .payments.update(item)
    .success(data => {
      if ($scope.$root.payment.objectId) {
        $('.modal.in').modal('toggle');
        location.reload();
      } else {
        console.error('Unable to save Agent data', data);
      }
    })
    .error(console.error.bind(console));
  };

  if ($scope.$root.admin && $scope.$root.admin.objectId) {
    $scope.load();
  }

}];
