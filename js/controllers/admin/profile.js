var DEFAULT_UPLOAD_URI = 'http://virtualmls.com/REBeacons/upload.aspx';

var ProfileController = module.exports = ["$http", "$scope", "$sce", "apiService", "profileService", function($http, $scope, $sce, apiService, profileService) {

  $scope.trustAsHtml = $sce.trustAsHtml;

  $scope.load = function() {
    profileService.get();
  };

  function refreshPhoto() {
    var el = $('.img-refresh:eq(0)');
    el.attr('src', el.attr('src'));
    $scope.status = 'Updating image...';
    $('.modal.in').modal('toggle');
    location.reload();
  }

  $('#admin_photo_upload').fileupload({
    multipart: true,
    paramName: 'file',
    forceIframeTransport: true,
    disableImageMetaDataLoad: true,
    dataType: 'json',
    maxFileSize: 25000000,
    acceptFileTypes: /(\.|\/)(gif|jpe?g|png|pdf|docx?|doc|xls.?|eps|ps)$/i,
    url: DEFAULT_UPLOAD_URI + '?name=avatars-' + $scope.$root.admin.objectId + '&index=1',
    always: refreshPhoto,
    formData: {}
  });

  $scope.editAdmin = function() {
    var item = $scope.$root.admin;
    return apiService
    .admins.update(item)
    .success(data => {
      if ($scope.$root.admin.objectId) {
        $('.modal.in').modal('toggle');
        location.reload();
      } else {
        console.error('Unable to save Admin data', data);
      }
    })
    .error(console.error.bind(console));
  };

  if ($scope.$root.admin && $scope.$root.admin.objectId) {
    $scope.load();
  }

}];
