var DEFAULT_UPLOAD_URI = 'http://virtualmls.com/REBeacons/upload.aspx';
var _ = require('lodash');
var BeaconController = module.exports = ["$scope", "$http", "$timeout", "beaconAdminService", "propertyAdminService", "agentAdminService", "profileService", "geoService", "paymentAdminService", function($scope, $http, $timeout, beaconAdminService, propertyAdminService, agentAdminService, profileService, geoService, paymentAdminService) {

  $scope.$root.currentIndex = 1; // For the photo upload to the server.

  $scope.pageLoading = function(yesNo) {
    var $target = $('.modal.in')
        .toggleClass('royal_preloader scrollreveal');
  };

  function checkValues(b) {
    b.photos = (new Array(10)).map((item, idx, arr) => typeof(b.photos[idx]) === 'string' ? b.photos[idx] : '<!-- FooBar -->');
    b.slides = (new Array(10)).map((item, idx, arr) => typeof(b.slides[idx]) === 'string' ? b.slides[idx] : '<!-- FooBar -->');
    return b;
  }

  $scope.$root.saveBeaconHtml = function(html) {
    console.warn('SLIDE.SELECTED', $scope.$root.currentIndex, html);
    $scope.$root.beacon.slides[$scope.$root.currentIndex] = html;
    $scope.$root.currentHtml = '[RESET]';
    return $scope.editBeacon($scope.$root.beacon);
  };

  $scope.load = function() {
    return beaconAdminService
    .get()
    .success(data => {
      $scope.beacons = data.results;
    })
    .error(console.error.bind(console));
  };

  $scope.loadProperties = function() {
    return propertyAdminService
    .get()
    .success(data => {
      $scope.properties = data.results;
    })
    .error(console.error.bind(console));
  };

  $scope.loadAgents = function() {
    return agentAdminService
    .get()
    .success(data => {
      $scope.agents = data.results;
    })
    .error(console.error.bind(console));
  };

  // Opens from the View/Edit Slide dropdown of 10 slides
  $scope.$root.openSlide = function(index, beacon){
    $scope.$root.beacon = beacon;
    $scope.$root.currentHtml = beacon.slides[index] || '<div><p>[Empty]</p></div>';
    $scope.$root.currentIndex = index;
    _updateFileName('beacons-' + beacon.objectId, 1 + index);
    console.log('Open slide', index, beacon.slides, beacon);
  };

  function _updateFileName(name, index) {
    $('#beacon_upload').fileupload('option', 'url', DEFAULT_UPLOAD_URI +
      '?name=' + encodeURIComponent(name) + '&index=' + encodeURIComponent(index));
  }

  $('#beacon_upload').fileupload({
    multipart: true,
    paramName: 'file',
    forceIframeTransport: true,
    disableImageMetaDataLoad: true,
    dataType: 'json',
    maxFileSize: 25000000,
    acceptFileTypes: /(\.|\/)(gif|jpe?g|png|pdf|docx?|doc|xls.?|eps|ps)$/i,
    url: DEFAULT_UPLOAD_URI + '?name=noslideselected',
    // always: avatarHandler,
    formData: {}
  });

  $scope.buyBeacon = function(payment) {
    payment.userPointer = apiService.getPointer($scope.$root.user.objectId);
    payment.agentPointer = apiService.getPointer($scope.$root.agent.objectId, 'Agents');
    payment.agentID = $scope.$root.agent.agentID;
    payment.status = 'P';
    if ( !payment.sponsorCode ) {
      payment.sponsorCode = 'None';
      payment.ownerPointer = undefined;
    }
    return paymentAdminService
    .save(payment)
    .success(data => {
      $scope.payment = data.results;
      $('.modal.in').modal('toggle');
    })
    .error(console.error.bind(console))
    .then(() => {
      $('#beaconSuccessModal').modal();
    });
  };

  $scope.editBeacon = function(beacon, result) {
    $scope.pageLoading(true);
    beacon = beacon || $scope.$root.beacon;
    beacon.userPointer = apiService.getPointer($scope.$root.user.objectId);
    beacon.agentPointer = apiService.getPointer($scope.$root.agent.objectId, 'Agents');
    beacon.agentID = $scope.$root.agent.agentID;
    beacon.major = $scope.$root.agent.major;
    beacon.propertyID = beacon.propertyPointer.propertyID;
    // beacon.latitude = beacon.propertyPointer.latitude;
    // beacon.longitude = beacon.propertyPointer.longitude;

    beacon.slides = _.isArray(beacon.slides) ? beacon.slides : [];
    if ( !beacon.slides ) {
      $scope.status = 'Please enter slide remarks';
      return null;
    }
    console.log('Editing beacon', beacon.slides, $scope.$root.currentIndex, $scope.$root.currentHtml);

    // var geoXhr = geoService.query(beacon.propertyPointer);

    beacon.propertyPointer = typeof(beacon.propertyPointer) === 'object' && beacon.propertyPointer.objectId ? beaconService.getPointer(beacon.propertyPointer.objectId, 'Properties') : beacon.propertyPointer;

    // geoXhr.then((latLon) => {
    //   beacon.latitude = (latLon.lat+'');
    //   beacon.longitude = (latLon.lon+'');
    // });

    return beaconAdminService
    .save(beacon)
    .then(() => {
      console.log('Saved property', beacon);
      $('.modal.in').modal('toggle');
      $scope.pageLoading(false);
      location.reload();
    });
  };

  $scope.openPopup = function($dialog) {
    $scope.$root.currentHtml = '';

    if ( typeof($dialog) === 'string' ) {
      $dialog = $($dialog);
    }
    var $currentModals = $('.modal.in');
    if ($currentModals.length > 0) { // if we have active modals
        $currentModals.one('hidden', function () {
            // when they've finished hiding
            $dialog.modal('show');
            $dialog.one('hidden', function () {
                // when we close the dialog
                $currentModals.modal('show');

            });
        }).modal('hide');
    } else { // otherwise just simply show the modal
        $dialog.modal('show');
    }
  };

  if ($scope.admin && $scope.admin.objectId) {
    $scope.load();
    $scope.loadProperties();
    $scope.loadAgents();
  }

}];