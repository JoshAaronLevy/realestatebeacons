var DEFAULT_UPLOAD_URI = 'http://virtualmls.com/REBeacons/upload.aspx';

var AgentController = module.exports = ["$http", "$scope", "$sce", "apiService", "agentAdminService", function($http, $scope, $sce, apiService, agentAdminService) {

  $scope.trustAsHtml = $sce.trustAsHtml;

  $scope.load = function() {
    agentAdminService.get();
  };

  // function refreshPhoto() {
  //   var el = $('.img-refresh:eq(0)');
  //   el.attr('src', el.attr('src'));
  //   $scope.status = 'Updating image...';
  //   $('.modal.in').modal('toggle');
  //   location.reload();
  // }

  // function refreshLogo() {
  //   var el = $('.img-refresh:eq(1)');
  //   el.attr('src', el.attr('src'));
  //   $scope.status = 'Updating image...';
  //   $('.modal.in').modal('toggle');
  //   location.reload();
  // }

  // $('#agent_photo_upload').fileupload({
  //   multipart: true,
  //   paramName: 'file',
  //   forceIframeTransport: true,
  //   disableImageMetaDataLoad: true,
  //   dataType: 'json',
  //   maxFileSize: 25000000,
  //   acceptFileTypes: /(\.|\/)(gif|jpe?g|png|pdf|docx?|doc|xls.?|eps|ps)$/i,
  //   url: DEFAULT_UPLOAD_URI + '?name=avatars-' + $scope.$root.agent.objectId + '&index=1',
  //   always: refreshPhoto,
  //   formData: {}
  // });

  // $('#agent_logo_upload').fileupload({
  //   multipart: true,
  //   paramName: 'file',
  //   forceIframeTransport: true,
  //   disableImageMetaDataLoad: true,
  //   dataType: 'json',
  //   maxFileSize: 25000000,
  //   acceptFileTypes: /(\.|\/)(gif|jpe?g|png|pdf|docx?|doc|xls.?|eps|ps)$/i,
  //   url: DEFAULT_UPLOAD_URI + '?name=logos-' + $scope.$root.agent.objectId + '&index=1',
  //   always: refreshLogo,
  //   formData: {}
  // });

  $scope.viewAgentModal = function(agent) {
    $scope.selectedAgent = agent;
  };

  $scope.modalPageReload = function() {
    self.location.reload();
  };

  $scope.editAgent = function() {
    var item = $scope.$root.agent;
    return apiService
    .agents.update(item)
    .success(data => {
      if ($scope.$root.agent.objectId) {
        $('.modal.in').modal('toggle');
        location.reload();
      } else {
        console.error('Unable to save Agent data', data);
      }
    })
    .error(console.error.bind(console));
  };

  if ($scope.$root.admin && $scope.$root.admin.objectId) {
    $scope.load();
  }

}];
