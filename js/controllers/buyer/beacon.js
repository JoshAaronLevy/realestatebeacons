var DEFAULT_UPLOAD_URI = 'http://virtualmls.com/REBeacons/upload.aspx';
var _ = require('lodash');
var BeaconController = module.exports = ["$scope", "$http", "$timeout", "beaconService", "propertyService", "agentService", "geoService", "paymentService", function($scope, $http, $timeout, beaconService, propertyService, agentService, geoService, paymentService) {

  $scope.$root.currentIndex = 1; // For the photo upload to the server.

  $scope.pageLoading = function(yesNo) {
    var $target = $('.modal.in')
        .toggleClass('royal_preloader scrollreveal');
  };

  function checkValues(b) {
    b.photos = (new Array(10)).map((item, idx, arr) => typeof(b.photos[idx]) === 'string' ? b.photos[idx] : '<!-- FooBar -->');
    b.slides = (new Array(10)).map((item, idx, arr) => typeof(b.slides[idx]) === 'string' ? b.slides[idx] : '<!-- FooBar -->');
    return b;
  }

  $scope.load = function() {
    return beaconService
    .get()
    .success(data => {
      $scope.beacons = data.results;
    })
    .error(console.error.bind(console));
  };

  $scope.loadProperties = function() {
    return propertyService
    .get()
    .success(data => {
      $scope.properties = data.results;
    })
    .error(console.error.bind(console));
  };

  $scope.loadAgents = function() {
    agentService.get();
  };

  // Opens from the View/Edit Slide dropdown of 10 slides
  $scope.$root.openSlide = function(index, beacon){
    $scope.$root.beacon = beacon;
    $scope.$root.currentHtml = beacon.slides[index] || '<div><p>[Empty]</p></div>';
    $scope.$root.currentIndex = index;
    _updateFileName('beacons-' + beacon.objectId, 1 + index);
    console.log('Open slide', index, beacon.slides, beacon);
  };

  $scope.buyBeacon = function(payment) {
    payment.userPointer = apiService.getPointer($scope.$root.user.objectId);
    payment.ownerPointer = apiService.getPointer($scope.$root.owner.objectId, 'Owners');
    payment.sponsorCode = $scope.$root.owner.ownerID;
    payment.sponsorApproved = 'Yes';
    payment.status = 'P';

    return paymentService
    .save(payment)
    .success(data => {
      $scope.payment = data.results;
      $('.modal.in').modal('toggle');
    })
    .error(console.error.bind(console))
    .then(() => {
      $('#beaconSponsorSuccessModal').modal();
    });
  };

  $scope.editBeacon = function(beacon, result) {
    $scope.pageLoading(true);
    beacon = beacon || $scope.$root.beacon;
    beacon.userPointer = apiService.getPointer($scope.$root.user.objectId);
    // beacon.agentPointer = apiService.getPointer($scope.$root.agent.objectId, 'Agents');
    beacon.ownerPointer = apiService.getPointer($scope.$root.owner.objectId, 'Owners');
    beacon.ownerID = beacon.ownerPointer.ownerID;
    // beacon.major = $scope.$root.owner.major;
    beacon.agentID = beacon.agentPointer.agentID;
    if (!beacon.propertyPointer) {
      beacon.propertyPointer = undefined;
    } else {
      beacon.propertyPointer = typeof(beacon.propertyPointer) === 'object' && beacon.propertyPointer.objectId ? beaconService.getPointer(beacon.propertyPointer.objectId, 'Properties') : beacon.propertyPointer;
    }

    beacon.agentPointer = typeof(beacon.agentPointer) === 'object' && beacon.agentPointer.objectId ? beaconService.getPointer(beacon.agentPointer.objectId, 'Agents') : beacon.agentPointer;

    console.log('Beacon details retrieved', beacon);
    return beaconService
    .save(beacon)
    .then(() => {
      console.log('Saved beacon', beacon);
      $('.modal.in').modal('toggle');
      $scope.pageLoading(false);
      // location.reload();
    });
  };

  $scope.modalPageReload = function() {
    self.location.reload();
  };

  $scope.openPopup = function($dialog) {
    $scope.$root.currentHtml = '';

    if ( typeof($dialog) === 'string' ) {
      $dialog = $($dialog);
    }
    var $currentModals = $('.modal.in');
    if ($currentModals.length > 0) { // if we have active modals
        $currentModals.one('hidden', function () {
            // when they've finished hiding
            $dialog.modal('show');
            $dialog.one('hidden', function () {
                // when we close the dialog
                $currentModals.modal('show');

            });
        }).modal('hide');
    } else { // otherwise just simply show the modal
        $dialog.modal('show');
    }
  };

  if ($scope.user && $scope.user.objectId) {
    $scope.load();
    $scope.loadProperties();
    $scope.loadAgents();
  }

}];