var _ = require('lodash');
var AuthController = module.exports = ["$scope", "$http", "$rootScope", "apiService", "authService", function($scope, $http, $rootScope, apiService, authService) {

  $scope.login = function(username, password) {
    localStorage.clear();
    var postData = {username: username || $scope.username, password: password || $scope.password};
    $scope.error = null;
    $scope.status = 'Logging in...';
    return apiService
    .auth.send(postData)
    .success(function(data) {
      console.warn(data);
      data = data.payload ? data.payload : data;
      if (!data || !data.objectId){
        $scope.error = 'Invalid login and password';
        console.error('Invalid server response', arguments);
      } else {
        $scope.user = data;
        $rootScope.user = data;
        localStorage.setItem('user', JSON.stringify(data));
        $scope.status = 'Retrieving account information. Please wait...';
      }
    })
    .error(errHandler)
    .then(findRole);
  };

  function findRole() {
    var user = $scope.user;
    var postCmd = 'where=' + escape(JSON.stringify({user: apiService.getPointer(user.objectId)}));
    console.log('authService', authService);
    if (authService.isAgent(user)){
      console.log('I am an Agent', user);
      return apiService
      .agents.get(postCmd)
      .success(data => {
        console.warn(data);
        data = data.payload ? data.payload : data;
        data = data.results && data.results.length > 0 ? data.results[0] : data;
        if (!data || !data.objectId){
          $scope.error = 'Invalid Agent';
          console.error('Invalid server response for Agent', arguments);
        } else if (user.status !== 'P') {
          $rootScope.agent = data;
          localStorage.setItem('agent', JSON.stringify(data));
          $scope.status = 'Successfully retrieved info!';
          window.location = '/agent/properties.html';
        } else if (user.status === 'P') {
          $rootScope.agent = data;
          localStorage.setItem('agent', JSON.stringify(data));
          $scope.status = 'Successfully retrieved info!';
          window.location = '/agent/confirmation.html';
        }
      });
    } else if (authService.isSponsor(user)){
      return apiService
      .owners.get(postCmd)
      .success(data => {
        console.warn(data);
        data = data.payload ? data.payload : data;
        data = data.results && data.results.length > 0 ? data.results[0] : data;
        if (!data || !data.objectId){
          $scope.error = 'Invalid Sponsor';
          console.error('Invalid server response for Sponsor', arguments);
        } else {
          $rootScope.owner = data;
          localStorage.setItem('owner', JSON.stringify(data));
          $scope.status = 'Successfully retrieved info!';
          window.location = '/sponsor/beacons.html';
        }
      });
    } else if (authService.isAdmin(user)){
      return apiService
      .admins.get(postCmd)
      .success(data => {
        console.warn(data);
        data = data.payload ? data.payload : data;
        data = data.results && data.results.length > 0 ? data.results[0] : data;
        if (!data || !data.objectId){
          $scope.error = 'Invalid Admin';
          console.error('Invalid server response for Admin', arguments);
        } else {
          $rootScope.admin = data;
          localStorage.setItem('admin', JSON.stringify(data));
          $scope.status = 'Successfully retrieved info!';
          window.location = '/admin/payments.html';
        }
      });
    } else {
      throw new Error('Unknown role.');
    }
  }

  var user = $scope.$root.user;

  function errHandler(err) {
    $scope.status = null;
    $scope.error = err && err.message || err;
  }

  $scope.agentSignup = function(username, password) {
    var uName = $scope.signupUser.username;
    var pWord = $scope.signupUser.password;
    var ag = _.extend({
      parent: apiService.getPointer($scope.$root.user.objectId, '_User'),
      displayName: String($scope.$root.user.firstName + ' ' + $scope.$root.user.lastName),
      companyName: $scope.$root.user.companyName,
      companyEmail: $scope.$root.user.username,
      agentID: $scope.$root.user.agentID,
    },$scope.signupUser);
    ag.passwordConfirm = undefined;
    return apiService
    .users.signup(ag)
    .success((data) => {
      data = data.payload ? data.payload : data;
      if (!data || !data.objectId){
        $scope.error = 'Unable to sign up. Please double check the fields above. If you have already signed up, please log in at the top.';
        console.error('Invalid server response', arguments);
      } else {
        $scope.$root.user = data;
        $rootScope.user = data;
        localStorage.setItem('user', JSON.stringify(data));
        $scope.status = 'Successfully signed up!';
      }
    })
    .error(errHandler)
    .then(results => {
      ag = _.extend({
        parent: apiService.getPointer($scope.$root.user.objectId, '_User'),
        displayName: String($scope.$root.user.firstName + ' ' + $scope.$root.user.lastName),
        companyName: $scope.$root.user.companyName,
        companyEmail: $scope.$root.user.username,
        agentID: $scope.$root.user.agentID,
      },ag);
      ag.referredBy = undefined;
      return apiService
      .agents.create(ag)
      .success(data => {
        console.warn(data);
        data = data.payload ? data.payload : data;
        if (!data || !data.objectId){
          $scope.error = 'Invalid login and password';
          console.error('Invalid server response', arguments);
        } else {
          $rootScope.agent = data;
          localStorage.setItem('agent', JSON.stringify(data));
          $scope.status = 'Successfully logged in!';
          window.location = '/agent/subscription.html';
        }
      })
      .error(console.error.bind(console))
      .then(() => window.self.location.href = '/agent/subscription.html');
    });
  };

  $scope.agentConfirm = function() {
    var major = Math.floor(Math.random()*8009) + 1001;
    var ag = $scope.$root.user;
    ag.status = 'A';
    return apiService
    .users.update(ag)
    .success((data) => {
      data = data.payload ? data.payload : data;
      if (!data || !data.objectId){
        $scope.error = 'Unable to sign up. Please double check the fields above. If you have already signed up, please log in at the top.';
        console.error('Invalid server response', arguments);
      } else {
        $scope.$root.user = data;
        $rootScope.user = data;
        localStorage.setItem('user', JSON.stringify(data));
        $scope.status = 'Successfully signed up!';
      }
    })
    .error(errHandler)
    .then(results => {
      ag = _.extend({
        user: apiService.getPointer($scope.$root.user.objectId, '_User'),
        agentDisplayName: String($scope.$root.user.firstName + ' ' + $scope.$root.user.lastName),
        agentOfficeName: $scope.$root.user.companyName,
        status: 'A',
        region: $scope.$root.user.region
      },ag);
      ag.major = String(major);
      ag.username = undefined;
      ag.password = undefined;
      ag.passwordConfirm = undefined;
      ag.firstName = undefined;
      ag.lastName = undefined;
      ag.companyName = undefined;
      ag.role = undefined;
      ag.termsAgreed = undefined;
      return apiService
      .agents.update(ag)
      .success(data => {
        console.warn(data);
        data = data.payload ? data.payload : data;
        if (!data || !data.objectId){
          $scope.error = 'Invalid login and password';
          console.error('Invalid server response', arguments);
        } else {
          $rootScope.agent = data;
          localStorage.setItem('agent', JSON.stringify(data));
          $scope.status = 'Successfully logged in!';
          // window.location = '/agent/profile.html';
        }
      })
      .error(console.error.bind(console))
      .then(() => window.self.location.href = '/agent/profile.html');
    });
  };

  $scope.$root.logout = function(username, password) {
    localStorage.clear();
    console.log('apiService', apiService);
    window.self.location.href = '/index.html?';
  };

  $scope.$root.logout2 = function(username, password) {
    localStorage.clear();
    console.log('apiService', apiService);
    window.self.location.href = '/login.html';
  };

  $scope.$root.logout3 = function(username, password) {
    localStorage.clear();
    console.log('apiService', apiService);
    window.self.location.href = '/signup.html';
  };

  function getUserPointer(objectId) {
    return {
      "__type": "Pointer",
      "className": "_User",
      "objectId": objectId
    };
  }

}];
