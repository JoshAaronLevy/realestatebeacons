var DEFAULT_UPLOAD_URI = 'http://virtualmls.com/REBeacons/upload.aspx';
var _ = require('lodash');
var ProfileController = module.exports = ["$http", "$scope", "$sce", "apiService", "profileService", "authService", function($http, $scope, $sce, apiService, profileService, authService) {

  $scope.trustAsHtml = $sce.trustAsHtml;

  $scope.load = function() {
    profileService.get();
    // tour.init();
  };

  // // Instance the tour
  // var tour = new Tour({
  //   steps: [
  //   {
  //     element: "#Profile",
  //     title: "Title of my step",
  //     content: "Content of my step"
  //   },
  //   {
  //     element: "#my-other-element",
  //     title: "Title of my step",
  //     content: "Content of my step"
  //   }
  // ]});

  // $scope.showTour = function() {
  //   tour.start();
  // };

  function refreshPhoto() {
    var el = $('.img-refresh:eq(0)');
    el.attr('src', el.attr('src'));
    $scope.status = 'Updating image...';
    $('.modal.in').modal('toggle');
    location.reload();
  }

  function refreshLogo() {
    var el = $('.img-refresh:eq(1)');
    el.attr('src', el.attr('src'));
    $scope.status = 'Updating image...';
    $('.modal.in').modal('toggle');
    location.reload();
  }

  $('#agent_photo_upload').fileupload({
    multipart: true,
    paramName: 'file',
    forceIframeTransport: true,
    disableImageMetaDataLoad: true,
    dataType: 'json',
    maxFileSize: 25000000,
    acceptFileTypes: /(\.|\/)(gif|jpe?g|png|pdf|docx?|doc|xls.?|eps|ps)$/i,
    url: DEFAULT_UPLOAD_URI + '?name=avatars-' + $scope.$root.agent.objectId + '&index=1',
    always: refreshPhoto,
    formData: {}
  });

  $('#agent_logo_upload').fileupload({
    multipart: true,
    paramName: 'file',
    forceIframeTransport: true,
    disableImageMetaDataLoad: true,
    dataType: 'json',
    maxFileSize: 25000000,
    acceptFileTypes: /(\.|\/)(gif|jpe?g|png|pdf|docx?|doc|xls.?|eps|ps)$/i,
    url: DEFAULT_UPLOAD_URI + '?name=logos-' + $scope.$root.agent.objectId + '&index=1',
    always: refreshLogo,
    formData: {}
  });

  $scope.editorOptions = {
    language: 'en',
    // uiColor: '#fff',
    toolbar: [
      { name: 'clipboard', items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
      { name: 'editing', items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'SpellChecker', 'Scayt' ] },
      { name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
      { name: 'insert', items: [ 'Table', 'SpecialChar', 'HorizontalRule' ] },
      { name: 'tools', items: [ 'Maximize' ] },
      { name: 'document', items: [ 'Source' ] },
      '/',
      { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', '-', 'RemoveFormat' ] },
      { name: 'paragraph', items: [ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote' ] },
      { name: 'styles', items: [ 'Styles', 'Format' ] }
    ]
  };

  $scope.editAgent = function() {
    var item = $scope.$root.agent;
    return apiService
    .agents.update(item)
    .success(data => {
      if ($scope.$root.agent.objectId) {
        $('.modal.in').modal('toggle');
        location.reload();
      } else {
        console.error('Unable to save Agent data', data);
      }
    })
    .error(console.error.bind(console));
  };

  if ($scope.$root.agent && $scope.$root.agent.objectId) {
    $scope.load();
  }

}];
