var DEFAULT_UPLOAD_URI = 'http://virtualmls.com/REBeacons/upload.aspx';
var _ = require('lodash');
var PaymentController = module.exports = ["$http", "$scope", "$sce", "apiService", "profileService", "authService", "paymentService", "paymentAgentService", function($http, $scope, $sce, apiService, profileService, authService, paymentService, paymentAgentService) {

  $scope.trustAsHtml = $sce.trustAsHtml;

  var user = $scope.$root.user;

  function errHandler(err) {
    $scope.status = null;
    $scope.error = err && err.message || err;
  }

  $scope.load = function() {
    $scope.setupStripe();
    paymentAgentService.get()
    .success(data => {
      $scope.payments = data.results;
    })
    .error(console.error.bind(console));
  };

  $scope.modalPageReload = function() {
    location.reload();
  };

  $scope.viewPaymentModal = function(payment) {
    $scope.selectedPayment = payment;
  };

  $scope.editPayment = function() {
    var item = $scope.$root.payment;
    apiService
    .payments.update(item)
    .success(data => $('.modal.in').modal('toggle'))
    .error(console.error.bind(console))
    .then(() => {
      $scope.load();
    });
  };

  $scope.buyBeacon = function(payment) {
    payment.userPointer = apiService.getPointer($scope.$root.user.objectId);
    payment.agentPointer = apiService.getPointer($scope.$root.agent.objectId, 'Agents');
    payment.agentID = $scope.$root.agent.agentID;
    payment.agentApproved = 'Yes';
    payment.status = 'P';
    if ( !payment.sponsorCode ) {
      payment.sponsorCode = 'None';
      payment.ownerPointer = undefined;
      payment.allClear = 'Yes';
    }
    return paymentService
    .save(payment)
    .success(data => {
      $scope.payment = data.results;
      $('.modal.in').modal('toggle');
    })
    .error(console.error.bind(console))
    .then(() => {
      $('#beaconSuccessModal').modal();
    });
  };

  $scope.approveOrder = function() {
    var payment = $scope.$root.payment;
    payment.agentPointer = apiService.getPointer($scope.$root.agent.objectId, 'Agents');
    payment.ownerPointer = undefined;
    payment.agentApproved = 'Yes';
    apiService
    .payments.update(payment)
    .success(data => $('.modal.in').modal('toggle'))
    .error(console.error.bind(console))
    .then(() => {
      $('#approveSuccessModal').modal();
    });
  };

  $scope.declineOrder = function() {
    var payment = $scope.$root.payment;
    payment.agentPointer = null;
    payment.ownerPointer = undefined;
    payment.agentApproved = 'Declined';
    apiService
    .payments.update(payment)
    .success(data => $('.modal.in').modal('toggle'))
    .error(console.error.bind(console))
    .then(() => {
      $('#declineSuccessModal').modal();
    });
  };

  // Stripe Payment Code
  $scope.setupStripe = function() {
    var pmt = $scope.$root.payment;
    $('#payment-form').submit(function(event) {
      var $form = $(this);

      // Disable the submit button to prevent repeated clicks
      $form.find('button').prop('disabled', true);

      Stripe.card.createToken({
        number: pmt.number,
        cvc: pmt.cvc,
        exp_month: pmt.expMonth,
        exp_year: pmt.expYear
      }, stripeResponseHandler);
      // Stripe.card.createToken($form, stripeResponseHandler);

      // Prevent the form from submitting with the default action
      return false;
    });
  }

  function stripeResponseHandler(status, response) {
    var $form = $('#payment-form');
    console.warn('Successfully saved card!', arguments);
    if (response.error) {
      // Show the errors on the form
      $form.find('.payment-errors').text(response.error.message);
      $form.find('button').prop('disabled', false);
    } else {
      // response contains id and card, which contains additional card details
      var token = response.id;
      // Insert the token into the form so it gets submitted to the server
      $form.append($('<input type="hidden" name="stripeToken" />').val(token));
      // and submit
      // $form.get(0).submit();
      var pmt = $scope.$root.payment;
      Parse.Cloud.run('payNow', {
        quantity: pmt.quantity,
        source: stripeToken,
        plan: "beacon-30",
        name: pmt.shippingAttn,
        email: pmt.email,
        shippingAddress1: pmt.shippingAddress1,
        shippingAddress2: pmt.shippingAddress2,
        shippingCity: pmt.shippingCity,
        shippingState: pmt.shippingState,
        shippingZip: pmt.shippingZip
      });
    }
  };

  if ($scope.agent && $scope.agent.objectId) {
    $scope.load();
  }

}];
