var DEFAULT_UPLOAD_URI = 'http://virtualmls.com/REBeacons/upload.aspx';
var _ = require('lodash');
var BeaconController = module.exports = ["$scope", "$http", "$timeout", "beaconService", "propertyService", "profileService", "geoService", "paymentService", function($scope, $http, $timeout, beaconService, propertyService, profileService, geoService, paymentService) {

  $scope.$root.currentIndex = 1; // For the photo upload to the server.

  $scope.pageLoading = function(yesNo) {
    var $target = $('.modal.in')
        .toggleClass('royal_preloader scrollreveal');
  };

  function checkValues(b) {
    b.photos = (new Array(10)).map((item, idx, arr) => typeof(b.photos[idx]) === 'string' ? b.photos[idx] : '<!-- FooBar -->');
    b.slides = (new Array(10)).map((item, idx, arr) => typeof(b.slides[idx]) === 'string' ? b.slides[idx] : '<!-- FooBar -->');
    return b;
  }

  $scope.$root.saveBeaconHtml = function(html) {
    console.warn('SLIDE.SELECTED', $scope.$root.currentIndex, html);
    $scope.$root.beacon.slides[$scope.$root.currentIndex] = html;
    $scope.$root.currentHtml = '[RESET]';
    return $scope.editBeacon($scope.$root.beacon);
  };

  $scope.load = function() {
    return beaconService
    .get()
    .success(data => {
      $scope.beacons = data.results;
    })
    .error(console.error.bind(console));
  };

  $scope.loadProperties = function() {
    return propertyService
    .get()
    .success(data => {
      $scope.properties = data.results;
    })
    .error(console.error.bind(console));
  };

  $scope.loadAgents = function() {
    return profileService
    .get()
    .success(data => {
      $scope.agents = data.results;
    })
    .error(console.error.bind(console));
  };

  // Opens from the View/Edit Slide dropdown of 10 slides
  $scope.$root.openSlide = function(index, beacon){
    $scope.$root.beacon = beacon;
    $scope.$root.currentHtml = beacon.slides[index] || '<div><p>[Empty]</p></div>';
    $scope.$root.currentIndex = index;
    _updateFileName('beacons-' + beacon.objectId, 1 + index);
    console.log('Open slide', index, beacon.slides, beacon);
  };

  function refreshBeaconPhoto() {
    $scope.status = 'Updating image...';
    $('.modal.in').modal('toggle');
    location.reload();
  }

  function _updateFileName(name, index) {
    $('#beacon_upload').fileupload('option', 'url', DEFAULT_UPLOAD_URI +
      '?name=' + encodeURIComponent(name) + '&index=' + encodeURIComponent(index));
  }

  $('#beacon_upload').fileupload({
    multipart: true,
    paramName: 'file',
    forceIframeTransport: true,
    disableImageMetaDataLoad: true,
    dataType: 'json',
    maxFileSize: 25000000,
    acceptFileTypes: /(\.|\/)(gif|jpe?g|png|pdf|docx?|doc|xls.?|eps|ps)$/i,
    url: DEFAULT_UPLOAD_URI + '?name=noslideselected',
    always: refreshBeaconPhoto,
    formData: {}
  });

  $scope.buyBeacon = function(payment) {
    payment.userPointer = apiService.getPointer($scope.$root.user.objectId);
    payment.agentPointer = apiService.getPointer($scope.$root.agent.objectId, 'Agents');
    payment.agentID = $scope.$root.agent.agentID;
    payment.agentApproved = 'Yes';
    payment.status = 'P';
    if ( !payment.sponsorCode ) {
      payment.sponsorCode = 'None';
      payment.ownerPointer = undefined;
      payment.allClear = 'Yes';
    }
    return paymentService
    .save(payment)
    .success(data => {
      $scope.payment = data.results;
      $('.modal.in').modal('toggle');
    })
    .error(console.error.bind(console))
    .then(() => {
      $('#beaconSuccessModal').modal();
    });
  };

  $scope.editorOptions = {
    language: 'en',
    // uiColor: '#fff',
    toolbar: [
      { name: 'clipboard', items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
      { name: 'editing', items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'SpellChecker', 'Scayt' ] },
      { name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
      { name: 'insert', items: [ 'Table', 'SpecialChar', 'HorizontalRule' ] },
      { name: 'tools', items: [ 'Maximize' ] },
      { name: 'document', items: [ 'Source' ] },
      '/',
      { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', '-', 'RemoveFormat' ] },
      { name: 'paragraph', items: [ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote' ] },
      { name: 'styles', items: [ 'Styles', 'Format' ] }
    ]
  };

  $scope.editBeacon = function(beacon, result) {
    $scope.pageLoading(true);
    beacon = beacon || $scope.$root.beacon;
    beacon.userPointer = apiService.getPointer($scope.$root.user.objectId);
    beacon.agentPointer = apiService.getPointer($scope.$root.agent.objectId, 'Agents');
    beacon.agentID = $scope.$root.agent.agentID;
    // beacon.major = $scope.$root.agent.major;
    beacon.propertyID = beacon.propertyPointer.propertyID;
    // beacon.latitude = beacon.propertyPointer.latitude;
    // beacon.longitude = beacon.propertyPointer.longitude;

    beacon.slides = _.isArray(beacon.slides) ? beacon.slides : [];
    if ( !beacon.slides ) {
      $scope.status = 'Please enter slide remarks';
      return null;
    }
    console.log('Editing beacon', beacon.slides, $scope.$root.currentIndex, $scope.$root.currentHtml);

    // var geoXhr = geoService.query(beacon.propertyPointer);

    beacon.propertyPointer = typeof(beacon.propertyPointer) === 'object' && beacon.propertyPointer.objectId ? beaconService.getPointer(beacon.propertyPointer.objectId, 'Properties') : beacon.propertyPointer;

    // geoXhr.then((latLon) => {
    //   beacon.latitude = (latLon.lat+'');
    //   beacon.longitude = (latLon.lon+'');
    // });

    return beaconService
    .save(beacon)
    .then(() => {
      console.log('Saved property', beacon);
      $('.modal.in').modal('toggle');
      $scope.pageLoading(false);
      location.reload();
    });
  };

  $scope.openPopup = function($dialog) {
    $scope.$root.currentHtml = '';

    if ( typeof($dialog) === 'string' ) {
      $dialog = $($dialog);
    }
    var $currentModals = $('.modal.in');
    if ($currentModals.length > 0) { // if we have active modals
        $currentModals.one('hidden', function () {
            // when they've finished hiding
            $dialog.modal('show');
            $dialog.one('hidden', function () {
                // when we close the dialog
                $currentModals.modal('show');

            });
        }).modal('hide');
    } else { // otherwise just simply show the modal
        $dialog.modal('show');
    }
  };

  if ($scope.user && $scope.user.objectId) {
    $scope.load();
    $scope.loadProperties();
    $scope.loadAgents();
  }

}];