var DEFAULT_UPLOAD_URI = 'http://virtualmls.com/REBeacons/upload.aspx';
var _ = require('lodash');
var PropertyController = module.exports = [
"$scope", "$http", "propertyService", "apiService", "geoService",
function($scope, $http, propertyService, apiService, geoService) {
  var remarksEditor;

  $scope.$root.property = {};

  $scope.$watchGroup(['property.address1', 'property.city', 'property.state', 'property.zip'], checkGeoCode);

  $scope.getFullAddress = geoService.getFullAddress;

  $scope.pageLoading = function(yesNo) {
    var $target = $('.modal.in')
    .toggleClass('royal_preloader scrollreveal');
  };

  // function initEditor() {
  //   if ( !CKEDITOR ) { throw new Error('Please include CKEDITOR js lib.'); }
  //   remarksEditor = CKEDITOR.replace('propertyRemarks');
  // }

  $scope.saveProperty = function(property) {
    $scope.pageLoading(true);

    var listingNum = Math.floor(Math.random()*800999) + 100001;

    property = property || $scope.$root.property;
    if ( !property.listDate ) {
      property.listDate = undefined;
    } else {
      property.listDate = String($scope.listedYear + '-' + $scope.listedMonth + '-' + $scope.listedDay);
    }
    // property.remarks = remarksEditor && remarksEditor.getData() || '';
    property.agent = propertyService.getPointer($scope.user.objectId);
    property.agentPointer = propertyService.getPointer($scope.agent.objectId, 'Agents');
    property.agentID = $scope.agent.agentID;
    if ( !property.propertyID ) {
      property.propertyID = String(listingNum);
    } else {
      property.propertyID = property.propertyID;
    }

    var result = propertyService.save(property);
    result
    .then(data => {
      $scope.pageLoading(false);
      location.reload();
    });
  };

  $scope.photoSelector = function(property, index) {
    $scope.$root.property = property;
    if (typeof(property.photoCount) === 'undefined'){
      property.photoCount = 0;
    }
    property.photoCount = (property.photoCount && property.photoCount === 0 ? 1 : property.photoCount);
    index = index || property.photos.length;
    _updateFileName('properties-' + property.objectId, property.photoCount + 1);
    $scope.status = 'Found ' + property.photoCount + ' photos';
    $scope.photos = [];
    for (var i = 1; i <= property.photoCount; i++) {
      $scope.photos.push('http://virtualmls.com/REBeacons/properties-' + property.objectId + '-' + i + '.jpg');
    }
  };

  function _updateFileName(name, index) {
    $('#property_upload').fileupload('option', 'url', DEFAULT_UPLOAD_URI +
      '?name=' + encodeURIComponent(name) + '&index=' + encodeURIComponent(index));
  }

  function savePhotoCount(index) {
    var prop = $scope.$root.property;
    if ( index !== null || typeof(prop.photoCount) === 'undefined' ) {
      prop.photoCount = index;
    } else {
      prop.photoCount = 1 + prop.photoCount;
    }
    return propertyService
    .save(prop)
    .then(data => {
      reloadCloudImages();
    })
    .then(() => {
      location.reload();
    });
  }

  function reloadCloudImages() {
    $('img.cloud-img').each(function(el) {
      el = $(el);
      el.attr('src', el.attr('src'));
    });
  }
  $('#property_upload').fileupload({
    multipart: true,
    paramName: 'file',
    forceIframeTransport: true,
    disableImageMetaDataLoad: true,
    dataType: 'json',
    maxFileSize: 25000000,
    acceptFileTypes: /(\.|\/)(gif|jpe?g|png|pdf|docx?|doc|xls.?|eps|ps)$/i,
    url: DEFAULT_UPLOAD_URI + '?name=noslideselected',
    always: savePhotoCount.bind(this, null),
    formData: {}
  });

  $scope.load = function() {
    return propertyService
    .get()
    .success(data => {
      $scope.properties = data.results;
    })
    .error(console.error.bind(console));
  };

  $scope.removeProperty = function(property) {
    return propertyService
    .remove(property)
    .then(() => {
      location.reload();
    });
  };

  $scope.editorOptions = {
    language: 'en',
    toolbar: [
      { name: 'clipboard', items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
      { name: 'editing', items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'SpellChecker', 'Scayt' ] },
      { name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
      { name: 'insert', items: [ 'Table', 'SpecialChar', 'HorizontalRule' ] },
      { name: 'tools', items: [ 'Maximize' ] },
      { name: 'document', items: [ 'Source' ] },
      '/',
      { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', '-', 'RemoveFormat' ] },
      { name: 'paragraph', items: [ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote' ] },
      { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
      { name: 'colors', items : [ 'TextColor' ] }
    ]
  };

  function checkGeoCode(oldVal, newVal){
    if (oldVal && newVal) {
      geoService
      .query(geoService.getFullAddress($scope.$root.property))
      .then((coords) => {
        console.log('GeoCode', coords);
        if ( coords && coords.lat && coords.lon ) {
          // TODO: check, might be backwards
          $scope.$root.property.latitude = String(coords.lat);
          $scope.$root.property.longitude = String(coords.lon);
        } else {
          $scope.$root.property.latitude = "0";
          $scope.$root.property.longitude = "0";
        }
      });
    }
  }

  if ($scope.user && $scope.user.objectId) {
    $scope.load();
  }

  // if ( !remarksEditor ) {
  //   initEditor();
  // }
  // PHOTO UPLOAD


}];
