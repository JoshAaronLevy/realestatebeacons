var DEFAULT_UPLOAD_URI = 'http://virtualmls.com/REBeacons/upload.aspx';
var _ = require('lodash');
var AgentController = module.exports = ["$http", "$scope", "$sce", "apiService", "agentService", "authService", function($http, $scope, $sce, apiService, agentService, authService) {

  $scope.trustAsHtml = $sce.trustAsHtml;

  var user = $scope.$root.user;

  function errHandler(err) {
    $scope.status = null;
    $scope.error = err && err.message || err;
  }

  $scope.load = function() {
    agentService.get();
  };

  $scope.signupAgent = function(username, password) {
    var ag = _.extend({
      role: apiService.getPointer('SmL59iJzxQ', '_Role'),
      status: 'P'
    },$scope.signupAgent);
    ag.passwordConfirm = undefined;
    user.status = 'P';
    return apiService
    .users.signup(ag)
    .success((data) => {
      data = data.payload ? data.payload : data;
      if (!data || !data.objectId){
        $scope.error = 'Unable to sign up. Please double check the fields above. If you have already signed up, please log in at the top.';
        console.error('Invalid server response', arguments);
      } else {
        $scope.$root.user = data;
        $scope.status = 'Successfully signed up!';
      }
    })
    .error(errHandler)
    .then(results => {
      ag = _.extend({
        user: apiService.getPointer($scope.$root.user.objectId, '_User'),
        sponsor: apiService.getPointer($scope.$root.owner.objectId, 'Owners'),
        ownerID: $scope.$root.owner.ownerID,
        email: $scope.$root.user.username,
        agentDisplayName: undefined,
        agentOfficeName: undefined,
        agentEmail: $scope.$root.user.username,
        agentID: $scope.$root.user.username,
        emailFlag: 'Y',
        status: 'P',
        region: undefined
      },ag);
      ag.major = undefined;
      ag.username = undefined;
      ag.password = undefined;
      ag.passwordConfirm = undefined;
      ag.firstName = undefined;
      ag.lastName = undefined;
      ag.companyName = undefined;
      ag.role = undefined;
      ag.termsAgreed = undefined;
      return apiService
      .agents.create(ag)
      .success(data => {
        console.warn(data);
        $('.modal.in').modal('toggle');
      })
      .error(console.error.bind(console))
      .then(() => {
        $('#sponsorNewAgentSuccessModal').modal();
      });
    });
  };

  $scope.modalPageReload = function() {
    self.location.reload();
  };

  $scope.viewAgentModal = function(agent) {
    $scope.selectedAgent = agent;
  };

  $scope.editAgent = function() {
    var item = $scope.$root.agent;
    apiService
    .agents.update(item)
    .success(data => $('.modal.in').modal('toggle'))
    .error(console.error.bind(console))
    .then(() => {
      $scope.load();
    });
  };

  if ($scope.owner && $scope.owner.objectId) {
    $scope.load();
  }

}];
