var DEFAULT_UPLOAD_URI = 'http://virtualmls.com/REBeacons/upload.aspx';
var _ = require('lodash');
var PaymentController = module.exports = ["$http", "$scope", "$sce", "apiService", "agentService", "authService", "paymentService", "paymentSponsorService", function($http, $scope, $sce, apiService, agentService, authService, paymentService, paymentSponsorService) {

  $scope.trustAsHtml = $sce.trustAsHtml;

  var user = $scope.$root.user;

  function errHandler(err) {
    $scope.status = null;
    $scope.error = err && err.message || err;
  }

  $scope.load = function() {
    paymentSponsorService.get()
    .success(data => {
      $scope.payments = data.results;
    })
    .error(console.error.bind(console));
  };

  $scope.modalPageReload = function() {
    location.reload();
  };

  $scope.viewPaymentModal = function(payment) {
    $scope.selectedPayment = payment;
  };

  $scope.buyBeacon = function(payment) {
    payment.userPointer = apiService.getPointer($scope.$root.user.objectId);
    payment.ownerPointer = apiService.getPointer($scope.$root.owner.objectId, 'Owners');
    payment.sponsorCode = $scope.$root.owner.ownerID;
    payment.sponsorApproved = 'Yes';
    payment.status = 'P';

    return paymentService
    .save(payment)
    .success(data => {
      $scope.payment = data.results;
      $('.modal.in').modal('toggle');
    })
    .error(console.error.bind(console))
    .then(() => {
      $('#beaconSponsorSuccessModal').modal();
    });
  };

  $scope.editPayment = function() {
    var item = $scope.$root.payment;
    apiService
    .payments.update(item)
    .success(data => $('.modal.in').modal('toggle'))
    .error(console.error.bind(console))
    .then(() => {
      $scope.load();
    });
  };

  $scope.approveOrder = function() {
    var payment = $scope.$root.payment;
    payment.agentPointer = undefined;
    payment.sponsorApproved = 'Yes';
    apiService
    .payments.update(payment)
    .success(data => $('.modal.in').modal('toggle'))
    .error(console.error.bind(console))
    .then(() => {
      $('#approveSuccessModal').modal();
    });
  };

  $scope.declineOrder = function() {
    var payment = $scope.$root.payment;
    payment.agentPointer = undefined;
    payment.ownerPointer = null;
    payment.sponsorApproved = 'Declined';
    apiService
    .payments.update(payment)
    .success(data => $('.modal.in').modal('toggle'))
    .error(console.error.bind(console))
    .then(() => {
      $('#declineSuccessModal').modal();
    });
  };

  if ($scope.owner && $scope.owner.objectId) {
    $scope.load();
  }

}];
