var DEFAULT_UPLOAD_URI = 'http://virtualmls.com/REBeacons/upload.aspx';

var ProfileController = module.exports = ["$http", "$scope", "$sce", "apiService", "profileService", function($http, $scope, $sce, apiService, profileService) {

  $scope.trustAsHtml = $sce.trustAsHtml;

  $scope.load = function() {
    profileService.get();
  };

  function refreshPhoto() {
    var el = $('.img-refresh:eq(0)');
    el.attr('src', el.attr('src'));
    $scope.status = 'Updating image...';
    $('.modal.in').modal('toggle');
    location.reload();
  }

  function refreshLogo() {
    var el = $('.img-refresh:eq(1)');
    el.attr('src', el.attr('src'));
    $scope.status = 'Updating image...';
    $('.modal.in').modal('toggle');
    location.reload();
  }

  $('#sponsor_photo_upload').fileupload({
    multipart: true,
    paramName: 'file',
    forceIframeTransport: true,
    disableImageMetaDataLoad: true,
    dataType: 'json',
    maxFileSize: 25000000,
    acceptFileTypes: /(\.|\/)(gif|jpe?g|png|pdf|docx?|doc|xls.?|eps|ps)$/i,
    url: DEFAULT_UPLOAD_URI + '?name=avatars-' + $scope.$root.owner.objectId + '&index=1',
    always: refreshPhoto,
    formData: {}
  });

  $('#sponsor_logo_upload').fileupload({
    multipart: true,
    paramName: 'file',
    forceIframeTransport: true,
    disableImageMetaDataLoad: true,
    dataType: 'json',
    maxFileSize: 25000000,
    acceptFileTypes: /(\.|\/)(gif|jpe?g|png|pdf|docx?|doc|xls.?|eps|ps)$/i,
    url: DEFAULT_UPLOAD_URI + '?name=logos-' + $scope.$root.owner.objectId + '&index=1',
    always: refreshLogo,
    formData: {}
  });

  $scope.editSponsor = function() {
    var item = $scope.$root.owner;
    return apiService
    .owners.update(item)
    .success(data => {
      if ($scope.$root.owner.objectId) {
        $('.modal.in').modal('toggle');
        location.reload();
      } else {
        console.error('Unable to save Sponsor data', data);
      }
    })
    .error(console.error.bind(console));
  };

  if ($scope.$root.owner && $scope.$root.owner.objectId) {
    $scope.load();
  }

}];
