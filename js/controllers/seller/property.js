var DEFAULT_UPLOAD_URI = 'http://virtualmls.com/REBeacons/upload.aspx';
var _ = require('lodash');
var PropertySponsorController = module.exports = [
"$scope", "$http", "propertySponsorService", "apiService", "geoService",
function($scope, $http, propertySponsorService, apiService, geoService) {

  $scope.$root.property = {};
  // $scope.property.propertyID = Math.random().toString().substr(1,6);

  $scope.$watchGroup(['property.address1', 'property.city', 'property.state', 'property.zip'], checkGeoCode);

  $scope.getFullAddress = geoService.getFullAddress;

  $scope.pageLoading = function(yesNo) {
    var $target = $('.modal.in')
    .toggleClass('royal_preloader scrollreveal');
  };

  $scope.saveProperty = function(property) {
    $scope.pageLoading(true);

    var listingNum = Math.floor(Math.random()*999999) + 128663;

    property = property || $scope.$root.property;
    if ( !property.listDate ) {
      property.listDate = undefined;
    } else {
      property.listDate = String($scope.listedYear + '-' + $scope.listedMonth + '-' + $scope.listedDay);
    }
    property.agent = propertySponsorService.getPointer($scope.user.objectId);
    property.agentPointer = propertySponsorService.getPointer($scope.agent.objectId, 'Agents');
    property.agentID = $scope.agent.agentID;
    if ( !property.propertyID ) {
      property.propertyID = String(listingNum);
    } else {
      property.propertyID = property.propertyID;
    }

    var result = propertySponsorService.save(property);
    // console.warn('Save promise:', result);
    result
    .then(data => {
      $scope.pageLoading(false);
      $scope.load();
    });
  };

  $scope.photoSelector = function(property, index) {
    $scope.$root.property = property;
    if (typeof(property.photoCount) === 'undefined'){
      property.photoCount = 1;
    }
    property.photoCount = (property.photoCount && property.photoCount === 0 ? 1 : property.photoCount);
    index = index || property.photos.length;
    _updateFileName('properties-' + property.objectId, property.photoCount + 1);
    $scope.status = 'Found ' + property.photoCount + ' photos';
    $scope.photos = [];
    for (var i = 1; i <= property.photoCount; i++) {
      $scope.photos.push('http://virtualmls.com/REBeacons/properties-' + property.objectId + '-' + i + '.jpg');
    }
  };

  function _updateFileName(name, index) {
    // index = index + 1;
    $('#property_upload').fileupload('option', 'url', DEFAULT_UPLOAD_URI +
      '?name=' + encodeURIComponent(name) + '&index=' + encodeURIComponent(index));
  }

  function savePhotoCount(index) {
    var prop = $scope.$root.property;
    if ( index !== null || typeof(prop.photoCount) === 'undefined' ) {
      prop.photoCount = index;
    } else {
      prop.photoCount = 1 + prop.photoCount;
    }
    propertySponsorService
    .save(prop)
    .then(data => {
      reloadCloudImages();
    });
  }
  function reloadCloudImages() {
    $('img.cloud-img').each(function(el) {
      el = $(el);
      el.attr('src', el.attr('src'));
    });
  }
  $('#property_upload').fileupload({
    multipart: true,
    paramName: 'file',
    forceIframeTransport: true,
    disableImageMetaDataLoad: true,
    dataType: 'json',
    maxFileSize: 25000000,
    acceptFileTypes: /(\.|\/)(gif|jpe?g|png|pdf|docx?|doc|xls.?|eps|ps)$/i,
    url: DEFAULT_UPLOAD_URI + '?name=noslideselected',
    always: savePhotoCount.bind(this, null),
    formData: {}
  });

  $scope.load = function() {
    propertySponsorService.get()
    .success(data => {
      $scope.properties = data.results;
    })
    .error(console.error.bind(console));
  };

  // $scope.editProperty = function(property) {
  //   $scope.$root.property = property;
  //   $('.modal.in').modal('toggle');
  // };

  $scope.removeProperty = function(property) {
    propertySponsorService.remove(property)
    .then(() => {
      document.location.reload(true);
    });
  };

  if ($scope.user && $scope.user.objectId) {
    $scope.load();
  }

  function checkGeoCode(oldVal, newVal){
    if (oldVal && newVal) {
      geoService
      .query(geoService.getFullAddress($scope.$root.property))
      .then((coords) => {
        console.log('GeoCode', coords);
        if ( coords && coords.lat && coords.lon ) {
          // TODO: check, might be backwards
          $scope.$root.property.latitude = String(coords.lat);
          $scope.$root.property.longitude = String(coords.lon);
        } else {
          $scope.$root.property.latitude = "0";
          $scope.$root.property.longitude = "0";
        }
      });
    }
  }

  // PHOTO UPLOAD


}];
