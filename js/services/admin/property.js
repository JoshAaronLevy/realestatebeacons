var Promise = require('bluebird');
var _ = require('lodash');
var config = require('../../../config');
var propertyAdminService = module.exports = ['$http', '$rootScope', '$timeout', 'apiService', 'geoService', function($http, $rootScope, $timeout, apiService, geoService) {

  var _cached = null;

  return {
    getPointer: apiService.getPointer,
    getFullAddress: geoService.getFullAddress,

    get: function _get(cb) {
      var postCmd = 'include=agentPointer&where=';
      return apiService
      .properties.get(postCmd)
      .success(data => {
        $rootScope.status = 'Loaded server data.';
        _cached = data.results;
        _cached = _cached.map(p => {
          p.photos = _.uniq(p.photos);
          p.slides = _.uniq(p.slides);
          return p;
        });
        if ( typeof(cb) === 'function' ) { return cb(null, _cached); }
        $timeout(() => _cached = false, 5000);
      }).error(console.error.bind(console));
    },
    save: function _save(property) {
      var self = this;
      property.photos = undefined;
      // GeoCode before save
      return geoService.query(geoService.getFullAddress(property))
      .then((coords) => {
        if ( coords && coords.lat && coords.lon ) {
          property.latitude = String(coords.lat);
          property.longitude = String(coords.lon);
        }
        // now, always do property save
        return apiService
        .properties[property.objectId ? 'update' : 'create'](property)
        .success(data => {
          $rootScope.status = 'Saved Successfully!';
          $('.modal.in').modal('toggle');
        })
        .error(console.error.bind(console))
        .then(() => {
          $('#propertySuccessModal').modal();
        });
      });
    },
    remove: function _remove(item) {
      return apiService
      .properties.remove(item || $rootScope.property)
      .success(data => $rootScope.status = 'Deleted Property')
      .error(console.error.bind(console));
    }
  };

}];
