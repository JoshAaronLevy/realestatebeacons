var config = require('../../../config');
var _ = require('lodash');
const TIMEOUT = config.localTimeout || 5000;

var beaconAdminService = module.exports = ['$http', '$rootScope', '$timeout', 'apiService', function($http, $rootScope, $timeout, apiService) {

  var _cached = null;

  return {
    getPointer: apiService.getPointer,

    get: function _get(cb) {
      var postCmd = 'include=agentPointer,ownerPointer,propertyPointer&where=';
      return apiService
      .beacons.get(postCmd)
      .success(data => {
        $rootScope.status = 'Loaded server data.';
        _cached = data.results;
        if ( typeof(cb) === 'function' ) { return cb(null, _cached); }
      }).error(console.error.bind(console));
    }.bind(this),
    save: function _save(item) {
      var self = this;
      return apiService
      .beacons[item.objectId ? 'update' : 'create'](item)
      .success(data => {
        $rootScope.status = 'Saved Successfully!';
        _cached = null;
      })
      .error(console.error.bind(console));
    }.bind(this),
    remove: function _remove(item) {
      return apiService
      .beacons.remove(item)
      .success(data => {
        $rootScope.status = 'Deleted beacon';
        _cached = null;
      }).error(console.error.bind(console));
    }.bind(this)
  };

}];
