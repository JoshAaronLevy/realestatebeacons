var _ = require('lodash');
var config = require('../../../config');

var agentAdminService = module.exports = ['$http', '$rootScope', 'apiService', function($http, $rootScope, apiService) {

  return {
    getPointer: apiService.getPointer,

    get: function(callback) {
      var postCmd = 'include=sponsor&where=';
      return apiService
      .agents.get(postCmd)
      .success(data => {
        $rootScope.agents = data && data.results;
        $rootScope.status = 'Loaded server data.';
        if ( typeof(cb) === 'function' ) { return cb(null, _cached); }
      }).error(console.error.bind(console));
    }.bind(this),
  };
}];
