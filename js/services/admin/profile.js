var _ = require('lodash');
var config = require('../../../config');

var profileService = module.exports = ['$http', '$rootScope', 'apiService', function($http, $rootScope, apiService) {

  return {
    getPointer: apiService.getPointer,

    get: function(callback) {
      var postCmd = 'where=' + escape(JSON.stringify({user: apiService.getPointer($rootScope.user.objectId)}));
      return apiService
      .admins.get(postCmd)
      .success(data => {
        console.warn(data);
        data = data.payload ? data.payload : data;
        data = data.results && data.results.length > 0 ? data.results[0] : data;
        if (!data || !data.objectId){
          console.error('Invalid server response for Admin', arguments);
        } else {
          $rootScope.admin = data;
          localStorage.setItem('admin', JSON.stringify(data));
        }
      });
    }
  };
}];
