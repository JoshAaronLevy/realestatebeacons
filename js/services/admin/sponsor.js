var _ = require('lodash');
var config = require('../../../config');

var sponsorAdminService = module.exports = ['$http', '$rootScope', 'apiService', function($http, $rootScope, apiService) {

  return {
    getPointer: apiService.getPointer,

    get: function(callback) {
      return apiService
      .owners.get()
      .success(data => {
        $rootScope.owners = data && data.results;
        $rootScope.status = 'Loaded server data.';
        if ( typeof(cb) === 'function' ) { return cb(null, _cached); }
      }).error(console.error.bind(console));
    }.bind(this),
  };
}];
