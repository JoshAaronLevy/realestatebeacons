var config = require('../../config');

const TIMEOUT = config.localTimeout || 5000;

var EstimoteService = module.exports = ['$rootScope', '$interval', function($rootScope, $interval) {
  // Dictionary of beacons.
  var beacons = {};
  // Timer that displays list of beacons.
  var updateTimer = null;

  var _cached = null;
  var _handler = function(dummy) { console.warn('\nWARN: Default Handler'); }


  function onReady() {
    // Start tracking beacons!
    startScan();
    // Display refresh timer.
    updateTimer = $interval(_handler, TIMEOUT);
  }

  function startScan(handler) {
    _handler = handler ? handler : _handler;

    function onBeaconsInRange(beaconInfo) {
      //console.log('onBeaconsRanged: ' + JSON.stringify(beaconInfo))
      beacons = beaconInfo.beacons
        .filter(beacon => beacon.rssi < 0)
        .map(beacon => {
          beacon.lastSeen = new Date();
          beacon.key = beacon.uuid + ':' + beacon.major + ':' + beacon.minor;
          return beacon;
        })
        .map(transforms.proximity)
        .map(transforms.distance)
        .map(transforms.rssiInfo);
    }

    // Request permission from user to access location info. This is needed on iOS 8.
    estimote.beacons.requestAlwaysAuthorization();
    // Start ranging beacons.
    estimote.beacons.startRangingBeaconsInRegion({}, // Empty region matches all beacons with the Estimote factory set UUID.
      onBeaconsInRange,
      console.warn.bind(console, 'Ranging beacons did fail: '));
  }

  // function displayBeaconList()
  // {
  //   // Clear beacon list.
  //   $('#found-beacons').empty();

  //   var timeNow = Date.now();

  //   // Update beacon list.
  //   $.each(beacons, function(key, beacon)
  //   {
  //     // Only show beacons that are updated during the last 60 seconds.
  //     if (beacon.timeStamp + 60000 > timeNow)
  //     {
  //       // Create tag to display beacon data.
  //       var element = $(
  //         '<li>'
  //         + 'Major: ' + beacon.major + '<br />'
  //         + 'Minor: ' + beacon.minor + '<br />'
  //         + proximityHTML(beacon)
  //         + distanceHTML(beacon)
  //         + rssiHTML(beacon)
  //         + '</li>'
  //       );

  //       $('#found-beacons').append(element);
  //     }
  //   });
  // }

  var transforms = {
    proximity: function proximity(beacon) {
      var proximity = beacon.proximity;
      if (!proximity) { return beacon; }

      var proximityNames = [
        'Unknown',
        'Immediate',
        'Near',
        'Far'];
      beacon.proximityDesc = proximityNames[proximity] || 'unKnown';
      return beacon;
    },
    distance: function distance(beacon) {
      var meters = beacon.distance;
      if (!meters) { return beacon; }

      var distance = (meters > 1) ?
          meters.toFixed(3) + ' m' :
          (meters * 100).toFixed(3) + ' cm';

      if (meters < 0) { distance = '?'; }

      beacon.distDesc = distance;
      return beacon;// 'Distance: ' + distance + '<br />'
    },
    rssiInfo: function rssiInfo(beacon) {
      var beaconColors = [
        'rgb(214,212,34)', // unknown
        'rgb(215,228,177)', // mint
        'rgb(165,213,209)', // ice
        'rgb(45,39,86)', // blueberry
        'rgb(200,200,200)', // white
        'rgb(200,200,200)', // transparent
      ];

      // Get color value.
      var color = beacon.color || 0;
      // Eliminate bad values (just in case).
      color = Math.max(0, color);
      color = Math.min(5, color);
      var rgb = beaconColors[color];

      // Map the RSSI value to a width in percent for the indicator.
      var rssiWidth = 1; // Used when RSSI is zero or greater.
      if  (beacon.rssi < -100)  { rssiWidth = 100; }
      else if (beacon.rssi < 0) { rssiWidth = 100 + beacon.rssi; }
      // Scale values since they tend to be a bit low.
      rssiWidth *= 1.5;

      beacon.rgb = rgb;
      beacon.signalPercent = rssiWidth;

      return beacon;
    },
    signal: function rssiInfo(beacon) {
      // Map the RSSI value to a width in percent for the indicator.
      var rssiWidth = 1; // Used when RSSI is zero or greater.
      if  (beacon.rssi < -100)  { rssiWidth = 100; }
      else if (beacon.rssi < 0) { rssiWidth = 100 + beacon.rssi; }
      // Scale values since they tend to be a bit low.
      rssiWidth *= 1.5;

      beacon.signal = rssiWidth;

      return beacon;
    }
  };

}];

