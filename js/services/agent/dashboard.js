var _ = require('lodash');
var config = require('../../../config');

var dashboardService = module.exports = ['$http', '$rootScope', 'apiService', function($http, $rootScope, apiService) {

  return {
    getPointer: apiService.getPointer,

    get: function(callback) {
      var postCmd = 'include=beaconPointer&where=' + escape(JSON.stringify({agentPointer: apiService.getPointer($rootScope.agent.objectId, 'Agents')}));
      return apiService
      .dashboard.get(postCmd)
      .then(data => {
        console.warn(data);
        data = data.data ? data.data : data;
        data = data.results && data.results.length > 0 ? data.results : data;
        if (!data){
          return Promise.reject('Invalid server response for Agent', arguments);
        } else {
          return data;
        }
      });
    }
  };
}];
