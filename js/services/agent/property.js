var Promise = require('bluebird');
var _ = require('lodash');
var config = require('../../../config');
var PropertyService = module.exports = ['$http', '$rootScope', '$timeout', 'apiService', 'geoService', function($http, $rootScope, $timeout, apiService, geoService) {

  var _cached = null;

  return {
    getPointer: apiService.getPointer,
    getFullAddress: geoService.getFullAddress,

    get: function _get(cb) {
      if ( _cached ) {
        if ( typeof(cb) === 'function' ) { return cb(null, _cached); }
        return Promise.resolve(_cached);
      }
      var postCmd = 'where=' + escape(JSON.stringify({agent: apiService.getPointer($rootScope.user.objectId)}));
      return apiService
      .properties.get(postCmd)
      .success(data => {
        $rootScope.status = 'Loaded server data.';
        _cached = data.results;
        _cached = _cached.map(p => {
          p.photos = _.uniq(p.photos);
          p.slides = _.uniq(p.slides);
          return p;
        });
        if ( typeof(cb) === 'function' ) { return cb(null, _cached); }
        // Ghetto Expire Cached Data - 5 SECS
        $timeout(() => _cached = false, 5000);
      }).error(console.error.bind(console));
    },
    save: function _save(property) {
      var self = this;
      property.photos = undefined;
      // GeoCode before save
      return geoService.query(geoService.getFullAddress(property))
      .then((coords) => {
        if ( coords && coords.lat && coords.lon ) {
          // TODO: check, might be backwards
          property.latitude = String(coords.lat);
          property.longitude = String(coords.lon);
        }
        // now, always do property save
        return apiService
        .properties[property.objectId ? 'update' : 'create'](property)
        .success(data => {
          $rootScope.status = 'Saved Successfully!';
          $('.modal.in').modal('toggle');
        })
        .error(console.error.bind(console))
        .then(() => {
          $('#propertySuccessModal').modal();
        });
      });
      // .then(() => self.load());
    },
    remove: function _remove(item) {
      return apiService
      .properties.remove(item || $rootScope.property)
      .success(data => $rootScope.status = 'Deleted Property')
      .error(console.error.bind(console));
    }
  };

}];
