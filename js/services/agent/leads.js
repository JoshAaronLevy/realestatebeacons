var config = require('../../../config');

const TIMEOUT = config.localTimeout || 5000;

var beaconService = module.exports = ['$http', '$rootScope', '$timeout', 'apiService', function($http, $rootScope, $timeout, apiService) {

  var _cached = null;

  return {
    getPointer: apiService.getPointer,

    get: function _get(cb) {
      if ( _cached ) {
        if ( typeof(cb) === 'function' ) { return cb(null, _cached); }
        return Promise.resolve(_cached);
      }
      var postCmd = 'include=propertyPointer&where=' + escape(JSON.stringify({agentPointer: apiService.getPointer($rootScope.agent.objectId, 'Agents')}));
      return apiService
      .beacons.get(postCmd)
      .success(data => {
        $rootScope.status = 'Loaded server data.';
        _cached = data.results;
        if ( typeof(cb) === 'function' ) { return cb(null, _cached); }
      }).error(console.error.bind(console));
    }.bind(this),
    save: function _save(item) {
      var self = this;
      return apiService
      .beacons[item.objectId ? 'update' : 'create'](item)
      .success(data => {
        $rootScope.status = 'Saved Successfully!';
        _cached = null;
      })
      .error(console.error.bind(console));
    }.bind(this),
    remove: function _remove(item) {
      return apiService
      .beacons.remove(item)
      .success(data => {
        $rootScope.status = 'Deleted beacon';
        _cached = null;
      }).error(console.error.bind(console));
    }.bind(this)
  };

}];
