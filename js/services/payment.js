var _ = require('lodash');
var config = require('../../config');
var paymentService = module.exports = ['$http', '$rootScope', '$timeout', 'apiService', function($http, $rootScope, $timeout, apiService) {

  var _cached = null;

  return {
    getPointer: apiService.getPointer,

    save: function _save(payment) {
      var self = this;
      var address = null;
      if ($rootScope.payment.shippingMatch === 'Yes') {
        $rootScope.payment.shippingAttn = $rootScope.payment.name;
        $rootScope.payment.shippingAddress1 = $rootScope.payment.address;
        $rootScope.payment.shippingAddress2 = $rootScope.payment.address2;
        $rootScope.payment.shippingCity = $rootScope.payment.city;
        $rootScope.payment.shippingState = $rootScope.payment.state;
        $rootScope.payment.shippingZip = $rootScope.payment.zip;
      } else {
        payment.address = undefined;
        payment.address2 = undefined;
        payment.city = undefined;
        payment.state = undefined;
        payment.zip = undefined;
      }
      payment.number = undefined;
      payment.cvc = undefined;
      payment.expMonth = undefined;
      payment.expYear = undefined;
      payment.address = undefined;
      payment.address2 = undefined;
      payment.city = undefined;
      payment.state = undefined;
      payment.zip = undefined;
      return apiService
      .payments.create(payment)
      .success(data => {
        $rootScope.status = 'Saved Successfully!';
      })
      .error(console.error.bind(console));
    }
      // .then(() => self.load());
  };
}];
