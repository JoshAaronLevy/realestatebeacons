var config = require('../../../config');
var _ = require('lodash');
const TIMEOUT = config.localTimeout || 5000;

var paymentSponsorService = module.exports = ['$http', '$rootScope', '$timeout', 'apiService', 'authService', function($http, $rootScope, $timeout, apiService, authService) {

  var _cached = null;

  function _filterUnapproved(data){
    data = data || _cached;
    $rootScope.unapproved = data.filter(payment => payment.sponsorApproved !== 'Yes');
    return $rootScope.unapproved;
  }

  return {
    getPointer: apiService.getPointer,

    filterUnapproved: _filterUnapproved.bind(this),
    get: function _get(cb) {
      var postCmd = 'include=agentPointer&where=' + escape(JSON.stringify({ownerPointer: apiService.getPointer($rootScope.owner.objectId, 'Owners')}));
      return apiService
      .payments.get(postCmd)
      .success(data => {
        $rootScope.status = 'Loaded server data.';
        _cached = data.results;
        _filterUnapproved();
        if ( typeof(cb) === 'function' ) { return cb(null, _cached); }
      }).error(console.error.bind(console));
    }.bind(this),
    save: function _save(item) {
      var self = this;
      return apiService
      .payments.update(item)
      .success(data => {
        $rootScope.status = 'Saved Successfully!';
        _cached = null;
      })
      .error(console.error.bind(console));
    }.bind(this),
    remove: function _remove(item) {
      return apiService
      .payments.remove(item)
      .success(data => {
        $rootScope.status = 'Beacon request rejected.';
        _cached = null;
      }).error(console.error.bind(console));
    }.bind(this)
  };

}];
