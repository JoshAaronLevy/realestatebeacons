var _ = require('lodash');
var config = require('../../../config');

var agentService = module.exports = ['$http', '$rootScope', 'apiService', 'authService', function($http, $rootScope, apiService, authService) {

  $rootScope.property = {};

  return {
    getPointer: apiService.getPointer,

    get: function _get(cb) {
      var postCmd = 'where=' + escape(JSON.stringify({sponsor: apiService.getPointer($rootScope.owner.objectId, 'Owners')}));
      return apiService
      .agents.get(postCmd)
      .success(data => {
        $rootScope.agents = data && data.results;
        $rootScope.status = 'Loaded server data.';
        if ( typeof(cb) === 'function' ) { return cb(null, _cached); }
      }).error(console.error.bind(console));
    }.bind(this),
  };
}];
