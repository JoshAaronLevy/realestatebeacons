var SlideController = module.exports = ["$scope", "$http", "apiService", function($scope, $http, apiService) {

  $scope.pageLoading = function(yesNo) {
    var $target = $('.modal.in')
    .toggleClass('royal_preloader scrollreveal');
  };

  function getPointer(objectId, className = '_User') {
    return { "__type": "Pointer",
      "className": className,
      "objectId": objectId };
  }

  $scope.load = function() {
    var postCmd = 'where=' + escape(JSON.stringify({beacon: getPointer($scope.beacon.objectId)}));
    apiService
    .slides.get(postCmd)
    .success(data => {
      $scope.status = 'success';
      $scope.results = data.results;
    })
    .error(console.error.bind(console));    
  };

  $scope.editSlides = function() {
    var item = $scope.$root.slide,
      postCmd = 'where=' + escape(JSON.stringify(getPointer(item)));
    // console.log('saving slide postCmd:', postCmd);
    apiService
    .slides.update(item)
    .success(data => $scope.status = JSON.stringify(data))
    .error(console.error.bind(console));
  };

  if ($scope.beacon && $scope.beacon.objectId) {
    $scope.load();
  }

}];