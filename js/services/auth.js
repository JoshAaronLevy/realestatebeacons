var AGENT_ROLES = ['SmL59iJzxQ'];
var SPONSOR_ROLES = ['m0lNawH6Dd'];
var ADMIN_ROLES = ['RCtDFjxZK9'];

var _ = require('lodash');
var config = require('../../config');
var authService = module.exports = ['$rootScope', function($rootScope) {

  return {
    isAgent: (user = $rootScope.user) => (user && AGENT_ROLES.indexOf(user.role.objectId) > -1) || $rootScope.user && AGENT_ROLES.indexOf($rootScope.user.role.objectId) > -1,
    isSponsor: (user = $rootScope.user) => (user && SPONSOR_ROLES.indexOf(user.role.objectId) > -1) || $rootScope.user && SPONSOR_ROLES.indexOf($rootScope.user.role.objectId) > -1,
    isAdmin: (user = $rootScope.user) => (user && ADMIN_ROLES.indexOf(user.role.objectId) > -1) || $rootScope.user && ADMIN_ROLES.indexOf($rootScope.user.role.objectId) > -1
  };
}];