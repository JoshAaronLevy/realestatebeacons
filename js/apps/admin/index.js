var _ = window._ ? window._ : require('lodash');
var app = window.app ? window.app : angular.module("app", ['ngCkeditor']);
window.app = app;

app
.factory('apiService', require('../../services/apiService'))
.factory('authService', require('../../services/auth'))

.factory('agentAdminService', require('../../services/admin/agent'))
.factory('profileService', require('../../services/admin/profile'))
.factory('propertyAdminService', require('../../services/admin/property'))
.factory('sponsorAdminService', require('../../services/admin/sponsor'))
.factory('geoService', require('../../services/geocode'))
.factory('paymentService', require('../../services/payment'))
.factory('paymentAdminService', require('../../services/admin/payment'))

.factory('paymentAgentService', require('../../services/agent/payment'))

.controller('AuthController', require('../../controllers/auth'))
.controller('MainController', require('../../controllers/main'))
.controller('AgentController', require('../../controllers/admin/agent'))
.controller('SponsorController', require('../../controllers/admin/sponsor'))
.controller('PaymentAdminController', require('../../controllers/admin/payment'))
.controller('ProfileController', require('../../controllers/admin/profile'))
.controller('PropertyController', require('../../controllers/admin/property'));
