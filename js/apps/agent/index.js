var _ = window._ ? window._ : require('lodash');
var app = window.app ? window.app : angular.module("app", ['ngCkeditor', 'agGrid']);
window.app = app;

app
.factory('apiService', require('../../services/apiService'))
.factory('authService', require('../../services/auth'))

// .factory('paymentAgentService', require('../../services/agent/payment'))
.factory('profileService', require('../../services/agent/profile'))
.factory('propertyService', require('../../services/agent/property'))
.factory('dashboardService', require('../../services/agent/dashboard'))
.factory('geoService', require('../../services/geocode'))
// .factory('paymentService', require('../../services/payment'))

// .factory('paymentAdminService', require('../../services/admin/payment'))

.controller('AuthController', require('../../controllers/auth'))
.controller('MainController', require('../../controllers/main'))
// .controller('PaymentController', require('../../controllers/agent/payment'))
.controller('ProfileController', require('../../controllers/agent/profile'))
.controller('PropertyController', require('../../controllers/agent/property'))
.controller('DashboardController', require('../../controllers/agent/dashboard'));
