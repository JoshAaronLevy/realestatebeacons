var _ = window._ ? window._ : require('lodash');
var app = window.app ? window.app : angular.module("app", []);
window.app = app;

app
.factory('apiService', require('../../services/apiService'))
.factory('authService', require('../../services/auth'))
.factory('paymentAgentService', require('../../services/agent/payment'))
.factory('paymentAdminService', require('../../services/admin/payment'))

.controller('AuthController', require('../../controllers/auth'))
.controller('MainController', require('../../controllers/main'));
