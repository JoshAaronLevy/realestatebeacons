
var _   = window._   ? window._   : require('lodash');
var app = window.app ? window.app : angular.module("app", []);
window.app = app;
window._ = _;

app
.factory('apiService', require('../services/apiService'))
.factory('profileService', require('../services/agent'))
.directive('beaconSlides', require('../directives/beacon-slides'))
.directive('uploadImage', require('../directives/upload-image'))
.controller('AuthController', require('../controllers/auth'))
.controller('MainController', require('../controllers/main'))

.constant('TestBeacon', {"UUID":"7683F478-FB75-4350-8FE6-908BD8F06480","agent":{"__type":"Pointer","className":"_User","objectId":"euHRD9s3bG"},"agentID":"alex@hillsoft.com","beaconColor":"Mint","beaconID":"b03","beaconName":"Mint","beaconObjectID":"0","beaconStatus":"A","beaconTitle":"Beacon Three","createdAt":"2015-08-29T22:12:22.054Z","latitude":"-33.936542","longitude":"18.378375","major":"33208","minor":"18796","objectId":"kh6EVrajq2","ownerID":"demo","property":{"__type":"Pointer","className":"Properties","objectId":"ExGqkkEHcv"},"propertyID":"333333","updatedAt":"2015-08-30T06:44:58.454Z"});


