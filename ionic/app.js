var _ = window._ ? window._ : require('lodash');
var app = window.app ? window.app : angular.module("app", []);
window.app = app;

app
.factory('apiService', require('../services/apiService'))

.factory('profileService', require('../services/agent'))
.factory('propertyService', require('../services/property'))
.factory('beaconService', require('../services/beacon'))
.factory('geoService', require('../services/geocode'))
.factory('estimoteService', require('../services/estimote'))

.controller('AdminController', require('../controllers/admin'))
.controller('AuthController', require('../controllers/auth'))
.controller('MainController', require('../controllers/main'))

.controller('PropertyImageController', require('../controllers/image/property'))
// .controller('BeaconImageController', require('../controllers/image/beacon'))
.controller('ProfileImageController', require('../controllers/image/profile'))
.controller('ProfileController', require('../controllers/agent'))
.controller('PropertyController', require('../controllers/property'))
.controller('BeaconController', require('../controllers/beacon'));
