var _ = require('lodash');
var isDevMode = process.env.NODE_ENV !== 'production'; // dev by default

var globalOptions = {
  siteName: 'Real Estate Beacons',
  copyright: 'Hillside Software, Inc. All rights reserved.',
  email: 'info@hillsoft.com',
  apiServer: 'http://beta.realestatebeacons.com:3000/'
};

module.exports = _.merge({},
  globalOptions,
  isDevMode ? require('./development') : require('./production'));