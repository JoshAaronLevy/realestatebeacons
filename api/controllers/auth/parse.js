var passport = require('passport');
var ParseStrategy = require('passport-parse');
var parse = require('parse').Parse;
var config = require('../../config');

parse.initialize(config.parse.appKey, config.parse.jsKey);
parse.User.enableUnsafeCurrentUser();
Parse.User.enableRevocableSession();

var parseStrategy = new ParseStrategy({parseClient: parse});

passport.use(parseStrategy);

exports.login = function(req,res,next) {
  passport.authenticate('parse', function(err, user, info) {
    if (err || !user) {
      return res.status(400).json({payload : {error: info}, message : info.message});
    }
    req.logIn(user, function(err) {
      if (err) return res.status(400).json({payload : {error: err}});
      return res.json({
        payload : req.user,
        message : "Authentication successful"
      });
    });
  })(req,res);
};

exports.logout = function(req, res) {
  req.logout();
  res.json({ message: 'You\'ve been logged out'});
};