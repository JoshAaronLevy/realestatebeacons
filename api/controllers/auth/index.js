var express = require('express');
var app = module.exports = express();
var bodyParser = require('body-parser');
var passport = require('passport');
var parse = require('./parse');

passport.serializeUser(function(user, done) {
  done(null, user._sessionToken);
});

passport.deserializeUser(function(token, done) {
  done(null, token);
});

app.all('/auth', bodyParser.json(), parse.login);