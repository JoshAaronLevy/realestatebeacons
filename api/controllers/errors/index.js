var errors = {};

errors.notFound = function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
};

errors.unhandledException = function(err, req, res, next) {
  res.status(err.status || 500);
  res.json({'error' : {
    message: err.message,
    error: err
  }});
};

module.exports = errors;