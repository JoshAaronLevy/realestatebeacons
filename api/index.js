var express = require('express');
var app = express();

//
//app dependencies
//

var morgan = require('morgan');
var cookieParser = require('cookie-parser');
var session = require('cookie-session');
var passport = require('passport');
var middleware = require('./controllers/middleware');
var errors = require('./controllers/errors');
var config = require('./config');

//
//app settings
//

app.set('title', config.title);
app.set('port', config.port);
app.use(cookieParser());

//
//app settings:production
//

app.use(session(config.session));
app.use(morgan(config.morgan));

// 
//app global middleware 
// 

app.use(middleware.allowAllCors);

//
//app session
//

app.use(passport.initialize());
app.use(passport.session());

//
//app mountables
//

app.use(require('./controllers/auth'));
app.use(require('./controllers/proxy'));

//
//app error handling
//
app.use(errors.notFound);
app.use(errors.unhandledException);

app.listen(app.get('port'), function () {
  console.log('%s is listening on port %d in %s mode', 
              app.get('title'), 
              app.get('port'),
              app.get('env'));
});