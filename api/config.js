module.exports = {
  title: 'beaconproxy',
  port: 3000,
  session: {
    name: 'bp',
    secret: 'super_secret_key_should_probably_be_changed',
    maxAge: 6500000
  },
  morgan: 'dev',
  parse: {
    appKey: process.env.PARSE_APP_KEY,
    restKey: process.env.PARSE_REST_KEY,
    jsKey: process.env.PARSE_JS_KEY
  }
};