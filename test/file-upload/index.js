/*
 * jQuery File Upload Plugin JS Example
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/* global $, window */
var app = angular.module('app', []);

var BASE_UPLOAD_URI = 'http://virtualmls.com/REBeacons/upload.aspx';

app.controller('MainController', ['$scope', function($scope) {
  var $uploadEl = $('#fileupload, form.fileupload');
  $scope.property = {};
  $scope.objectId = ['Properties', $scope.property.objectId, '.jpg'].join('-');
  $scope.listingIndex = 1;

  $scope.updateFileName = function(id, index) {
    $scope.property.objectId = id;
    $scope.property.listingIndex = index;
    // Set URL w/ QueryString Params
    $uploadEl.fileupload('option', 'url', BASE_UPLOAD_URI +
      '?name=' + escape(id) + '&index=' + escape(index));
    // $uploadEl.fileupload('option', 'headers', {
    //   'X-NAME': id,
    //   'X-INDEX': index
    // });
  };


  $uploadEl.fileupload({
    // Uncomment the following to send cross-domain cookies:
    // xhrFields: {withCredentials: true},
    multipart: true,
    paramName: 'file',
    forceIframeTransport: true,
    disableImageMetaDataLoad: true,
    dataType: 'json',
    maxFileSize: 25000000,
    acceptFileTypes: /(\.|\/)(gif|jpe?g|png|pdf|docx?|doc|xls.?|eps|ps)$/i,
    url: BASE_UPLOAD_URI,
    always: showResult,
    formData: {}
    // formData: function(form) {
    //   var formData = new FormData();

    //   formData.append("name", $('#mlsID').val());
    //   formData.append("number", $('#mlsIndex').val());
    //   return formData;
    // }
  })
  .bind('fileuploadprocessdone', showResult)
  .bind('fileuploadprocessfail', showResult.bind($uploadEl, 'Failed to upload'))
  .bind('fileuploadprocessalways', showResult.bind($uploadEl, 'Uploaded!'));

  function showResult(e, data) {
    $('ul.files').append('<li>' + JSON.stringify(data) + '</li>\n' +
      '<li>' + JSON.stringify(e) + '</li>');
  }
}]);

  // Initialize the jQuery File Upload widget:

  // // Enable iframe cross-domain access via redirect option:
  // $('#fileupload').fileupload(
  //   'option',
  //   'redirect',
  //   window.location.href.replace(
  //     /\/[^\/]*$/,
  //     '/cors/result.html?%s'
  //   )
  // );

  // if (window.location.hostname === 'blueimp.github.io') {
  //   // Demo settings:
  //   $('#fileupload').fileupload('option', {
  //     url: '//jquery-file-upload.appspot.com/',
  //     // Enable image resizing, except for Android and Opera,
  //     // which actually support image resizing, but fail to
  //     // send Blob objects via XHR requests:
  //     disableImageResize: /Android(?!.*Chrome)|Opera/
  //       .test(window.navigator.userAgent),
  //     maxFileSize: 999000,
  //     acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i
  //   });
  //   // Upload server status check for browsers with CORS support:
  //   if ($.support.cors) {
  //     $.ajax({
  //       url: '//jquery-file-upload.appspot.com/',
  //       type: 'HEAD'
  //     }).fail(function () {
  //       $('<div class="alert alert-danger"/>')
  //         .text('Upload server currently unavailable - ' +
  //             new Date())
  //         .appendTo('#fileupload');
  //     });
  //   }
  // } else {
  //   // Load existing files:
  //   // $('#fileupload').addClass('fileupload-processing');
  // }
